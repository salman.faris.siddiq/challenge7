const { user } = require("../models");

module.exports = async (req, res, next) => {
    await user.findAll()
    const admin = req.user.isAdmin
    // console.log(admin)
    // console.log(users)
  if (admin === 1) {
    return next();
    
  } else {
    res.redirect("/error");
  }
};
