const { user, item } = require("../models");
const passport = require("../lib/passport");

module.exports = {
  register: (req, res, next) => {
    user
      .register(req.body)
      .then(() => {
        res.redirect("./login");
      })
      .catch((err) => next(err));
  },
  login: passport.authenticate("local", {
    successRedirect: "/whoami",
    failureRedirect: "/login",
    failureFlash: true,
  }),

  whoami: async (req, res) => {
    const users = req.user;
    const userProfile = await item
      .findAll({
        include: "profile",
      })
      .catch((error) => console.log(error));
    res.render("whoami", { users: users, userProfile: userProfile });
  },
  dashboard: async (req, res) => {
    const users = await user.findAll({});
    res.render("dashboard", { users: users });
  },
  delete: async (req, res) => {
    const { id } = req.params;
    await user
      .destroy({
        where: {
          id: id,
        },
        raw: true,
      })
      .catch((error) => console.log(error));

    res.redirect("/dashboard");
  },
  create: async (req, res) => {
    const { idbarang_user, nama, harga, kondisi } = await req.body;
    const userModel = item
      .create({
        idbarang_user: idbarang_user,
        nama: nama,
        harga: harga,
        kondisi: kondisi,
      })
      .catch((error) => console.log(error));
    res.redirect("/whoami");
  },
  update: async (req, res) => {
    await user.findAll()
    const idUser = req.user.id;
    const { username } = req.body;
    await user.update(
      {
        username: username,
      },
      {
        where: {
          id: idUser,
        },
      }
    ).catch((error) => console.log(error));
    res.redirect("/whoami");
  },
};
