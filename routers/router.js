const express = require("express");
const router = express.Router();
const auth = require("../controllers/authController");
const restrict = require("../middlewares/restrict");
const restrictSuper = require("../middlewares/superAdmin");


router.get('/',(req,res)=>res.render("home"))
router.get("/register", (req, res) => res.render("register"));
router.post("/register", auth.register);
router.get("/login", (req, res) => res.render("login"));
router.post("/login", auth.login);
router.get("/whoami", restrict, auth.whoami);
router.get("/dashboard", restrictSuper, auth.dashboard);
router.get("/logout", (req, res) => {
  req.logOut();
  req.session.destroy();
  res.redirect("./login");
});
router.get("/delete/:id", auth.delete);
router.post('/card', auth.create)
router.post('/baru',auth.update)
router.get('/error',(req,res)=> res.render('errordashboard'))


module.exports = router;
