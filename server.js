const express = require("express")
const app = express()
const port = 8000
const session = require("express-session")
const flash = require("express-flash")


app.use(express.static("public"))
app.use(express.json())
app.use(express.urlencoded({extended:false}))

app.use(session({
    secret:'binar',
    resave:false,
    saveUninitialized:false
}))

app.use(flash())

const passport = require('./lib/passport')
app.use(passport.initialize())
app.use(passport.session())


app.set('view engine', 'ejs')
app.use(express.static('public'))

const router = require('./routers/router')
app.use(router)







app.listen(port,()=>console.log('App berjalan di port '+port))